\documentclass{beamer}

%
% Packages
%

\usepackage{etex}               % No room for new \dimen

\usepackage{ctable}             % Booktabs-style tables
\usepackage{dirtree}            % Directory tree
\usepackage{graphicx}           % Images + colours
\usepackage{hyperref}           % Hyperlinks + URLs
\usepackage{listings}           % Source listings
\usepackage{marvosym}           % Smiley

%
% Beamer setup
%

\usetheme{Boadilla}

\setbeamercovered{invisible}

\setbeamertemplate{footline}{}            % No footer
\setbeamertemplate{navigation symbols}{}  % No navigation symbols
\setbeamertemplate{caption}{\centering\footnotesize\insertcaption\par}

\setbeamerfont{itemize/enumerate       body}{}
\setbeamerfont{itemize/enumerate    subbody}{size=\footnotesize}
\setbeamerfont{itemize/enumerate subsubbody}{size=  \scriptsize}
\setbeamersize{text margin left=2em, text margin right=2em}

%
% Other setup
%

% Order of preference for image formats with same basename
\graphicspath{{img/}}
\DeclareGraphicsExtensions{.pdf, .png, .jpg}

% Hyperlinks
\definecolor{byzantine}{HTML}{BD33A4}
\hypersetup{
  colorlinks,
  urlcolor = byzantine
}

% Listings
\lstset{
  basicstyle = \ttfamily,
  language   = Lisp
}

% Dirtree
\renewcommand*{\DTstyle}{\TTF}
\DTsetlength{0.2em}{0.65em}{0.2em}{0.1em}{0.1em}

%
% Macros
%

% Colours
\newcommand*{\NC}{\usebeamercolor[fg]{normal text}}
\newcommand*{\FC}{\usebeamercolor[fg]{frametitle}}

% Fonts
\newcommand*{\TTF}{\FC\ttfamily}
\newcommand*{\TT}[1]{{\TTF #1}}
\newcommand*{\TN}[1]{{\NC\normalfont #1}}

% Punctuation
\newcommand*{\TAT}{\textasciitilde}
\newcommand*{\OR}{{\NC$|$\ }}

% URLs
\newcommand*{\HR}[2]{\texttt{\href{#1}{#2}}}
\newcommand*{\SmallURL}[1]{{\small \url{#1}}}
\newcommand*{\Mailto}[1]{\href{mailto:#1}{\url{#1}}}

% List items
\newcommand*{\TTi}[1]{\item \TT{#1}}
\newcommand*{\TTI}[1]{\Item \TT{#1}}
\newcommand*{\HRi}[2]{\item \HR{#1}{#2}}
\newcommand*{\D}[1]{\TN{- #1}}

% Deeply nested itemize
\newcommand*{\Item}{\item[$\bullet$]}
\newenvironment*{Itemize}{\begin{description}[a]}{\end{description}}

% Environments
\newenvironment*{Frame}[1]{\begin{frame}\frametitle{#1}}{\end{frame}}
\newenvironment*{Table}[3]{\ctable[caption=#1]{#2}{}{#3}}{}
\newenvironment*{Figure}{\begin{figure}\centering\tiny}{\end{figure}}

%
% Front matter
%

\title{Introduction to Emacs}
\author{Basil L. Contovounesios \\ \Mailto{blc@netsoc.tcd.ie}}
\institute{For the Dublin University Internet Society %
           [\HR{https://www.netsoc.tcd.ie/}{TCD Netsoc}]}
\titlegraphic{\includegraphics[height=0.3\textheight]{netsoc-logo}}

%
% Document
%

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{Frame}{Summary}
  \begin{itemize}\itemsep1em
  \item Built around LISP interpreter
  \item Small C kernel
  \item (Emacs) Lisp layers all the way up
    \begin{itemize}
    \item First very-high-level language
    \end{itemize}
  \item Distillation of Lisp Machine environment
    \begin{itemize}
    \item Debugger, documentation, etc.
    \end{itemize}
  \item Macros and key bindings
  \item Ubiquitous
  \end{itemize}
\end{Frame}

\begin{Frame}{Disclaimer}
  \begin{itemize}\itemsep1em
  \item This is not me
    \begin{Figure}
      \includegraphics[height=0.6\textheight]{meditate}             \\[1em]
      \href{http://www.gnu.org/graphics/meditate.en.html}
           {``Levitating, Meditating, Flute-playing Gnu''}          \\
      Copyright \textcopyright\ 2001 Free Software Foundation, Inc. \\
      \includegraphics[height=\baselineskip]{gfdl-logo}
    \end{Figure}
  \end{itemize}
\end{Frame}

\begin{Frame}{Disclaimer}
  \begin{itemize}\itemsep1em
  \item Emacs is not the solution to everything
  \item There exist smaller binaries and runtimes than Emacs
  \item We will be focussing on GNU Emacs
  \end{itemize}
\end{Frame}

\begin{Frame}{History!}
  \begin{columns}
    \begin{column}{0.48\textwidth}
      \begin{itemize}
      \item First appeared in 1976, \\ same year as \TT{vi}
      \item Main author is \\ Richard M. Stallman
      \item First GNU project
      \end{itemize}
    \end{column}
    \begin{column}{0.48\textwidth}
      \begin{Figure}
        \includegraphics[height=0.75\textheight]{dance} \\
        via \url{https://stallman.org/articles/on-hacking.html}
      \end{Figure}
    \end{column}
  \end{columns}
\end{Frame}

\begin{Frame}{Space-cadet Keyboard}
  \begin{Figure}
    \includegraphics[width=\textwidth]{space-cadet} \\[0.5em]
    \href{https://commons.wikimedia.org/wiki/File:Space-cadet.jpg}
         {``Symbolics LM-2 Lisp Machine Space-cadet Keyboard''}
    by David Fischer \\[0.5em]
    \href{https://creativecommons.org/licenses/by-sa/3.0/deed.en}
         {\includegraphics[height=\baselineskip]{by-sa}}
  \end{Figure}
\end{Frame}

\begin{Frame}{Lear Siegler ADM-3A Terminal}
  \begin{Figure}
    \includegraphics[height=0.7\textheight]{adm3a} \\[0.5em]
    \href{https://commons.wikimedia.org/wiki/File:Adm3aimage.jpg}
         {``ADM-3A Mint-Blue (Rare) with Keypad''}
    by Chris Jacobs \\[0.5em]
    \href{https://creativecommons.org/licenses/by-sa/3.0/deed.en}
         {\includegraphics[height=\baselineskip]{by-sa}}
  \end{Figure}
\end{Frame}

\begin{Frame}{S.O.S.}
  \Table{Modifier keys}{llllll}{
    \TT{C} & Control & \TT{M} & Meta/Alt & \TT{S} & Shift
  }
  \vspace{-1.5em}
  \Table{Help}{>{\TTF}ll}{
    C-h   & Help/documentation prefix (also \TT{M-x help})    \NN
    C-h t & Tutorial \emph{\alert{\small highly recommended}} \NN
    C-h a & About/startup screen                              \NN[0.5em]
    C-h k & Describe key                                      \NN
    C-h f & Describe function                                 \NN
    C-h v & Describe variable                                 \NN
    C-h m & Describe mode                                     \NN
    C-h b & List key bindings
  }
\end{Frame}

\begin{Frame}{S.O.S.}
  \Table{On second thought}{>{\TTF}ll}{
    C-u \OR C-/ \OR C-\_ & Undo \NN
    C-g                  & I REGRET EVERYTHING
  }
\end{Frame}

\begin{Frame}{Terminology}
  \dirtree{%
    .1 Frame             \D{graphical system-level ``window'' or
                            single terminal screen                     }.
    .2 Minibuffer      \ \D{expression input and output echo area      }.
    .2 Window\ \ \ \ \ \ \D{frame section displaying single buffer     }.
    .3 Mode Line         \D{buffer status bar                          }.
    .3 Buffer      \ \ \ \D{text container                             }.
    .4 Point         \ \ \D{cursor position and region delimiter       }.
    .4 Mark        \ \ \ \D{the anti-point                             }.
    .4 Mode        \ \ \ \D{(major/minor) determines editing behaviour }.
    .5 Key bindings      \D{key sequence to function mapping           }.
    .5 Font lock   \ \ \ \D{syntax highlighting                        }.
    .5 Indentation.
  }
\end{Frame}

% Alternative format:

% \begin{Frame}{Terminology}
%   \begin{Itemize}
%     \TTI{Frame} - graphical system-level ``window'' or single terminal screen
%     \vspace{0.5em}
%     \begin{Itemize}
%       \TTI{Minibuffer\ }     - expression input and output echo area
%       \TTI{Window\ \ \ \ \ } - frame section displaying single buffer
%       \vspace{0.5em}
%       \begin{Itemize}
%         \TTI{Mode line}      - buffer status bar
%         \TTI{Buffer\ \ \ }   - text container
%         \vspace{0.5em}
%         \begin{Itemize}
%           \TTI{Point\ \ }    - cursor position and region delimiter
%           \TTI{Mark\ \ \ }   - another position and region delimiter
%           \TTI{Mode\ \ \ }   - (major/minor) - determines editing behaviour
%           \vspace{0.5em}
%           \begin{Itemize}
%             \TTI{Key bindings}    - key sequence to function mapping
%             \TTI{Font lock\ \ \ } - syntax highlighting
%             \TTI{Indentation}
%           \end{Itemize}
%         \end{Itemize}
%       \end{Itemize}
%     \end{Itemize}
%   \end{Itemize}
% \end{Frame}

\begin{Frame}{About Screen}
  \centering
  \includegraphics[width=0.94\textwidth]{emacs-startup}
\end{Frame}

\begin{Frame}{\TT{*scratch*} Buffer}
  \centering
  \includegraphics[width=0.94\textwidth]{emacs-scratch}
\end{Frame}

\begin{Frame}{More Commands!}
  \Table{}{>{\TTF}ll}{
    C-x C-c       & Save buffers \& quit        \NN
    C-x C-f       & Find file                   \NN
    C-x C-s       & Save buffer                 \NN
    C-x C-w       & Save buffer as              \NN[1em]
    C-a           & Beginning of line           \NN
    C-e           & End of line                 \NN
    C-k           & Kill until end of line      \NN[1em]
    C-SPC \OR C-@ & Set mark                    \NN
    M-w           & Copy/save region            \NN
    C-w           & Cut/kill region             \NN
    C-y           & Paste/yank last killed text
  }
\end{Frame}

\begin{Frame}{More Commands!}
  \Table{}{>{\TTF}ll}{
    C-s           & Incremental forward  search \NN
    C-r           & Incremental backward search \NN[1em]
    C-x <L/R>     & Previous/next buffer        \NN[1em]
    C-x 0         & Delete curent window        \NN
    C-x 1         & Maximise current window     \NN
    C-x 2         & Split window below          \NN
    C-x 3         & Split window right          \NN
    C-x o         & Select next window
  }
\end{Frame}

\begin{frame}[fragile]
  \frametitle{Customisation}
  \begin{itemize}
  \item Init file: \TT{\TAT/.emacs}, \TT{\TAT/.emacs.el}, \TT{\TAT/.emacs.d/init.el}
    \TTi{M-x customize}
    \TTi{M-x list-packages}
  \end{itemize}
  \begin{exampleblock}{Package Archive}
    \small
    Packages are limited to Emacs Lisp Package Archive (ELPA) by default. \\
    Extend to Milkypostman's ELPA (MELPA):
    \begin{lstlisting}
(require `package)
(add-to-list
 `package-archives
 `("melpa" . "http://melpa.org/packages/"))
    \end{lstlisting}
  \end{exampleblock}
\end{frame}

\begin{Frame}{Major Modes}
  \small
  \renewcommand*{\DTstyle}{\ttfamily}
  \dirtree{%
    .1 Fundamental Mode.
    .2 Text modes\ \ \ \ \ \ \ \ \D{\TT{text-mode}, \TT{html-mode},
                                    \TT{tex-mode}\ldots}.
    .2 Programming modes         \D{\TT{c-mode}, \TT{fortran-mode},
                                    \TT{haskell-mode}\ldots}.
    .2 Process modes     \ \ \ \ \D{\TT{mail-mode}, \TT{eshell-mode},
                                    \TT{dired-mode}\ldots}.
  }
  \vspace{2em}
  Every mode should have a \TT{hook}! \\
  Shame on those who never customise. \Smiley
\end{Frame}

\begin{Frame}{Some Nifty Packages}
  \Table{Built-in}{>{\TTF}ll}{
    \href{http://orgmode.org/}{org-mode}
                & Incredibly flexible and programmable notebooks \NN
    dired       & File explorer \NN
    tramp       & Remote/root file access \NN
    epa         & GnuPG interface \NN
    speedbar    & Navigation bar for files or source modules \NN
    eww \OR w3m & Web browser \NN
    ido         & Interactive completions in the minibuffer \NN
    vc          & Various version control utilities \NN
    eshell      & Shell-like command interpreter
  }
\end{Frame}

\begin{Frame}{Some Nifty Packages}
  \Table{Additional}{>{\TTF}ll}{
    \href{https://www.gnu.org/software/auctex/}{auctex}
      & Extensive TeX support \NN
    \href{https://github.com/jwiegley/use-package}{use-package}
      & Package configuration + key binding utilities \NN
    \href{http://magit.vc/}{magit}
      & Complete git interface \NN
    \href{https://www.emacswiki.org/emacs/MiniMap}{minimap}
      & Source viewing panel \`a la Sublime Text/Atom \NN
    \href{https://github.com/magnars/multiple-cursors.el}{multiple-cursors}
      & What it says \NN
    \href{https://www.emacswiki.org/emacs/CoffeeMode}{coffee-mode}
      &  \href{http://tools.ietf.org/html/rfc2324}{RFC 2324} BREW requests
  }
  \vspace{1em}
  And support for most languages under the sun!
\end{Frame}

\begin{Frame}{Original Emacs Manual Cover}
  \begin{Figure}
    \includegraphics[height=0.8\textheight]{its-cover} \\
    via \url{http://www.gnu.org/software/emacs/}
  \end{Figure}
\end{Frame}

\begin{Frame}{Fun}
  \begin{Figure}
    \includegraphics[width=\textwidth]{emacs-work} \\[0.5em]
    \href{http://fav.me/d38aj2x}{``Emacs User at Work''} by EarlColour \\[0.5em]
    \href{http://creativecommons.org/licenses/by-nc-nd/3.0/}
         {\includegraphics[height=\baselineskip]{by-nc-nd}}
  \end{Figure}
\end{Frame}

\begin{Frame}{Fun}
  \begin{Figure}
    \includegraphics[width=\textwidth]{real_programmers}   \\[0.5em]
    \href{https://xkcd.com/378/}{``Real Programmers''}
    by Randall Munroe on \href{http://xkcd.com/}{xkcd.com} \\[0.5em]
    \href{http://creativecommons.org/licenses/by-nc/2.5/}
         {\includegraphics[height=\baselineskip]{by-nc}}
  \end{Figure}
\end{Frame}

\begin{Frame}{Fun}
  \begin{Figure}
    \includegraphics[height=0.8\textheight]{workflow}      \\[0.5em]
    \href{https://xkcd.com/1172/}{``Workflow''}
    by Randall Munroe on \href{http://xkcd.com/}{xkcd.com} \\[0.5em]
    \href{http://creativecommons.org/licenses/by-nc/2.5/}
         {\includegraphics[height=\baselineskip]{by-nc}}
  \end{Figure}
\end{Frame}

\begin{Frame}{Fun}
  \begin{itemize}
    \TTi{M-x butterfly}
    \TTi{M-x animate-birthday-present}
    \TTi{M-x tetris}
    \TTi{M-x snake}
    \TTi{M-x doctor}
    \TTi{M-x hanoi}
    \HRi{https://www.gnu.org/software/emacs/manual/html_node/emacs/Mail-Amusements.html}
        {M-x spook}
    \TTi{M-x 2048-game} (requires \TT{2048-game} package)
  \end{itemize}
\end{Frame}

\begin{Frame}{Nethack}
  \begin{Figure}
    \includegraphics[width=0.9\textwidth]{nethack} \\[0.5em]
    via \url{http://www.nongnu.org/nethack-el/}
  \end{Figure}
\end{Frame}

\begin{Frame}{Further Information}
  \begin{block}{GNU Emacs Manual}
    \SmallURL{https://www.gnu.org/software/emacs/manual/}
  \end{block}
  \begin{block}{Information on packages and how to get started}
    \SmallURL{https://www.emacswiki.org/emacs/SiteMap}
  \end{block}
  \begin{block}{Cheat sheet}
    \SmallURL{https://www.gnu.org/software/emacs/refcards/pdf/refcard.pdf}
  \end{block}
\end{Frame}

\begin{Frame}{Further Information}
  \begin{block}{Package list}
    \SmallURL{https://github.com/emacs-tw/awesome-emacs}
  \end{block}
  \begin{block}{Feature screencast series}
    \SmallURL{http://emacsrocks.com/}
  \end{block}
  \begin{block}{For purists}
    \SmallURL{https://github.com/magnars/hardcore-mode.el}
  \end{block}
  \begin{block}{Search online for sample init files, or look at mine}
    \SmallURL{https://github.com/basil-conto/dotfiles/}
  \end{block}
\end{Frame}

\begin{frame}
  \title{}
  \author{Basil L. Contovounesios    \\
          \Mailto{blc@netsoc.tcd.ie} \\
          \url{http://blc.netsoc.ie/slides/}}
  \institute{}
  \date{}
  \titlepage
\end{frame}

\end{document}
